import string
import datetime

# implementation of https://en.wikipedia.org/wiki/Machine-readable_passport
# ToDo
# - add verification of double filler between surname and given names, single filler between filler names

class PassportCheckDigits:
    def __init__(self, line1=44*'<', line2=44*'<'):
        self.line1 = line1
        self.line2 = line2
        self.legal_line2_characters = string.digits + '<' + string.ascii_uppercase  # sorted list of allowed characters
        self.legal_line1_characters = '<' + string.ascii_uppercase  # sorted list of allowed characters


    def isCharLegal(self, c, legalchars):
        # binary search on legal characters
        def find(c, i, j):
            if i == j:
                return c == legalchars[i]
            elif c < legalchars[i] or c > legalchars[j]:
                return False
            else:
                m = (i + j) // 2
                cm = legalchars[m]
                if c <= cm:
                    return find(c, i, m)
                else:
                    return find(c, m + 1, j)
        return find(c, 0, len(legalchars) - 1)

    # verify if the characters in s are legal. line == 1 for line 1 and line 2 otherwise
    def areLineCharactersLegal(self, s, line):
        if line == 1:
            legalcharacters = self.legal_line1_characters
        else:
            legalcharacters = self.legal_line2_characters
        for c in s:
            if not self.isCharLegal(c, legalcharacters):
                return False
        return True

    def check_digit(self, s):
        weights = [7, 3, 1]
        i = 0
        accumulator = 0
        for c in s:
            o = ord(c)
            # val = 0
            if o < 58:  # digit
                val = o - 48
            elif o < 61:  # filler <
                val = 0
            else:  # letter
                val = 10 + o - 65
            accumulator += weights[i % 3] * val
            i += 1  # cycle through the weights
        return accumulator % 10

    def validate(self):
        # line lengths perhaps not hardcoded
        if len(self.line1) != 44:
            return False, f"Line 1 was not 44 characters long, instead it was {len(self.line1)}"
        if len(self.line2) != 44:
            return False, f"Line 2 was not 44 characters long, instead it was {len(self.line2)}"
        if not self.areLineCharactersLegal(self.line1, 1):
            return False, f"Line 1 contains illegal characters, should only be A..Z or <, instead: {self.line1}"
        if not self.areLineCharactersLegal(self.line2, 2):
            return False, f"Line 2 contains illegal characters, should only be A..Z, 0..9, or <, instead: {self.line2}"
        try:
            cd = int(self.line2[9])
        except:
            return False, f"Character 10 of line 2 was not a digit, instead it was: {self.line2[9]}"
        ccd = self.check_digit(self.line2[0:9])
        if cd != ccd:
            return False, f"The first check digit of line 2 is wrong, it's {cd} and I expected {ccd}"
        try:
            dob_str = self.line2[13:19]
            datetime.datetime.strptime(dob_str, "%y%m%d")
        except:
            return False, f"I expected a date of birth, instead I got {dob_str}"
        try:
            cd = int(self.line2[19])
        except:
            return False, f"Character 20 of line 2 was not a digit, instead it was: {self.line2[19]}"
        ccd = self.check_digit(self.line2[13:19])
        if cd != ccd:
            return False, f"The second check digit of line 2 is wrong, it's {cd} and I expected {ccd}"
        sex = self.line2[20]
        if sex not in "FM<":
            return False, f"I expected M, F, or < but I received {sex} in position 21 of line 2"
        try:
            exp_str = self.line2[21:27]
            datetime.datetime.strptime(exp_str, "%y%m%d")
        except:
            return False, f"I expected an expiration date, instead I got {exp_str}"
        try:
            cd = int(self.line2[27])
        except:
            return False, f"Character 28 of line 2 was not a digit, instead it was: {self.line2[27]}"
        ccd = self.check_digit(self.line2[21:27])
        if cd != ccd:
            return False, f"The third check digit of line 2 is wrong, it's {cd} and I expected {ccd}"
        if self.line2[28:43] != 15*'<':
            try:
                cd = int(self.line2[42])
            except:
                return False, f"Character 43 of line 2 was not a digit, instead it was: {self.line2[42]}"
            ccd = self.check_digit(self.line2[28:42])
            if cd != ccd:
                return False, f"The fourth check digit of line 2 is wrong, it's {cd} and I expected {ccd}"
        try:
            cd = int(self.line2[43])
        except:
            return False, f"Character 44 of line 2 was not a digit, instead it was: {self.line2[43]}"
        ccd = self.check_digit(self.line2[0:10] + self.line2[13:20] + self.line2[21:43])
        if cd != ccd:
            return False, f"The fifth check digit of line 2 is wrong, it's {cd} and I expected {ccd}"
        return True, None




