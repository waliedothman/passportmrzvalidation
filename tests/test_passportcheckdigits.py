import pytest
from ..src.passportcheckdigits import PassportCheckDigits


class TestMRZ:
    def test_valid(self):
        cd = PassportCheckDigits(line1="P<IDNWIRYAWAN<<RUSMIN<<<<<<<<<<<<<<<<<<<<<<<",
                                 line2="B6667270<5IDN5204261M2203274<<<<<<<<<<<<<<04")
        b, _ = cd.validate()
        assert b

    def test_case1(self):
        cd = PassportCheckDigits(line1="P<IDNWIRYAWAN<<RUSMIN<<<<<<<<<<<<<<<<<<<<<<",
                                 line2="B6667270<5IDN5204261M2203274<<<<<<<<<<<<<<04")
        b, s = cd.validate()
        assert not b and s == "Line 1 was not 44 characters long, instead it was 43"

    def test_case2(self):
        cd = PassportCheckDigits(line1="P<IDNWIRYAWAN<<RUSMIN<<<<<<<<<<<<<<<<<<<<<<<",
                                 line2="B6667270<5IDN5204261M2203274<<<<<<<<<<<<<04")
        b, s = cd.validate()
        assert not b and s == "Line 2 was not 44 characters long, instead it was 43"

    def test_case3(self):
        cd = PassportCheckDigits(line1="P<IDNWIRYAWAN<<RUSMIN<<<<<<<<<<<<<<<<<?<<<<<",
                                 line2="B6667270<5IDN5204261M2203274<<<<<<<<<<<<<<04")
        b, s = cd.validate()
        assert not b and s == "Line 1 contains illegal characters, should only be A..Z or <, instead: P<IDNWIRYAWAN<<RUSMIN<<<<<<<<<<<<<<<<<?<<<<<"

    def test_case4(self):
        cd = PassportCheckDigits(line1="P<IDNWIRYAWAN<<RUSMIN<<<<<<<<<<<<<<<<<<<<<<<",
                                 line2="B6667270<5IDN5204261M2203274<<<<<<<<<?<<<<04")
        b, s = cd.validate()
        assert not b and s == "Line 2 contains illegal characters, should only be A..Z, 0..9, or <, instead: B6667270<5IDN5204261M2203274<<<<<<<<<?<<<<04"

    def test_case5(self):
        cd = PassportCheckDigits(line1="P<IDNWIRYAWAN<<RUSMIN<<<<<<<<<<<<<<<<<<<<<<<",
                                 line2="B6667270<AIDN5204261M2203274<<<<<<<<<<<<<<04")
        b, s = cd.validate()
        assert not b and s == "Character 10 of line 2 was not a digit, instead it was: A"

    def test_case6(self):
        cd = PassportCheckDigits(line1="P<IDNWIRYAWAN<<RUSMIN<<<<<<<<<<<<<<<<<<<<<<<",
                                 line2="B6667270<4IDN5204261M2203274<<<<<<<<<<<<<<04")
        b, s = cd.validate()
        assert not b and s == "The first check digit of line 2 is wrong, it's 4 and I expected 5"

    def test_case7(self):
        cd = PassportCheckDigits(line1="P<IDNWIRYAWAN<<RUSMIN<<<<<<<<<<<<<<<<<<<<<<<",
                                 line2="B6667270<5IDN5213261M2203274<<<<<<<<<<<<<<04")
        b, s = cd.validate()
        assert not b and s == "I expected a date of birth, instead I got 521326"

    def test_case8(self):
        cd = PassportCheckDigits(line1="P<IDNWIRYAWAN<<RUSMIN<<<<<<<<<<<<<<<<<<<<<<<",
                                 line2="B6667270<5IDN520426AM2203274<<<<<<<<<<<<<<04")
        b, s = cd.validate()
        assert not b and s == "Character 20 of line 2 was not a digit, instead it was: A"

    def test_case9(self):
        cd = PassportCheckDigits(line1="P<IDNWIRYAWAN<<RUSMIN<<<<<<<<<<<<<<<<<<<<<<<",
                                 line2="B6667270<5IDN5204261G2203274<<<<<<<<<<<<<<04")
        b, s = cd.validate()
        assert not b and s == "I expected M, F, or < but I received G in position 21 of line 2"

    def test_case10(self):
        cd = PassportCheckDigits(line1="P<IDNWIRYAWAN<<RUSMIN<<<<<<<<<<<<<<<<<<<<<<<",
                                 line2="B6667270<5IDN5204261M2223274<<<<<<<<<<<<<<04")
        b, s = cd.validate()
        assert not b and s == "I expected an expiration date, instead I got 222327"

    def test_case11(self):
        cd = PassportCheckDigits(line1="P<IDNWIRYAWAN<<RUSMIN<<<<<<<<<<<<<<<<<<<<<<<",
                                 line2="B6667270<5IDN5204261M220327S<<<<<<<<<<<<<<04")
        b, s = cd.validate()
        assert not b and s == "Character 28 of line 2 was not a digit, instead it was: S"

    def test_case12(self):
        cd = PassportCheckDigits(line1="P<IDNWIRYAWAN<<RUSMIN<<<<<<<<<<<<<<<<<<<<<<<",
                                 line2="B6667270<5IDN5204261M2203275<<<<<<<<<<<<<<04")
        b, s = cd.validate()
        assert not b and s == "The third check digit of line 2 is wrong, it's 5 and I expected 4"

    def test_case13(self):
        cd = PassportCheckDigits(line1="P<IDNWIRYAWAN<<RUSMIN<<<<<<<<<<<<<<<<<<<<<<<",
                                 line2="B6667270<5IDN5204261M2203274<<<<<<<<<<<<<<D4")
        b, s = cd.validate()
        assert not b and s == "Character 43 of line 2 was not a digit, instead it was: D"

    def test_case14(self):
        cd = PassportCheckDigits(line1="P<IDNWIRYAWAN<<RUSMIN<<<<<<<<<<<<<<<<<<<<<<<",
                                 line2="B6667270<5IDN5204261M2203274<<<<<<<<<<<<<<0I")
        b, s = cd.validate()
        assert not b and s == "Character 44 of line 2 was not a digit, instead it was: I"

    def test_case15(self):
        cd = PassportCheckDigits(line1="P<IDNWIRYAWAN<<RUSMIN<<<<<<<<<<<<<<<<<<<<<<<",
                                 line2="B6667270<5IDN5204261M2203274<<<<<<<<<<<<<<05")
        b, s = cd.validate()
        assert not b and s == "The fifth check digit of line 2 is wrong, it's 5 and I expected 4"
    # def test_fail(self):
    #     cd = PassportCheckDigits(line1="P<IDNWIRYAWAN<<RUSMIN<<<<<<<<<<<<<<<<<<<<<<<",
    #                              line2="B6667270<5IDN5204261M2203274<<<<<<<<<<<<<<04")
    #     b,s = cd.validate()
    #     assert not b
