A tool to validate the Machine Readable Zone (MRZ) of passports, currently only supporting a 2 line MRZ described [here](https://en.wikipedia.org/wiki/Machine-readable_passport).

In addition, this repository demonstrates how to include tests and continuous integration to your own repositories. In particular, we use [pytest](https://docs.pytest.org/en/latest/contents.html) for testing, and [BitBucket Pipelines](https://www.atlassian.com/continuous-delivery/tutorials/continuous-integration-tutorial) for Continuous Integration (CI).
See the ```bitbucket-pipelines.yml``` file for additional details.

Now, with every commit, BitBucket runs your tests and alerts you if they fail.

To run tests locally, make sure to ```pip3 install pytest``` and then run ```pytest``` in the root of this repository.
